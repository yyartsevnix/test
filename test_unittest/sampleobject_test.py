from unittest import TestCase
from requests_test.sampleobj import get_change_set
import re


class TestSampleObject(TestCase):
    pattern = re.compile(r"Name = ([\w\s]+); X = (\d+); Y = (\d+\.?\d*)")

    def test_int(self):
        result = get_change_set(6)
        match = self.pattern.search(result)
        groups = match.groups()
        self.assertEqual(groups[0], "TestComplete Sample")
        self.assertEqual(groups[1], str(6 ** 2))
        self.assertAlmostEqual(float(groups[2]), 6 ** 0.5, delta=0.01)

    def test_float(self):
        result = get_change_set(8.3)
        match = self.pattern.search(result)
        groups = match.groups()
        self.assertEqual(groups[0], "TestComplete Sample")
        self.assertEqual(groups[1], str(8 ** 2))
        self.assertAlmostEqual(float(groups[2]), 8 ** 0.5, delta=0.01)

    def test_int_str(self):
        result = get_change_set("1994")
        match = self.pattern.search(result)
        groups = match.groups()
        self.assertEqual(groups[0], "TestComplete Sample")
        self.assertEqual(groups[1], str(1994 ** 2))
        self.assertAlmostEqual(float(groups[2]), 1994 ** 0.5, delta=0.01)

    def test_illegal(self):
        self.assertRaises(ValueError, get_change_set, "inf")
