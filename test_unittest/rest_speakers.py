from unittest import TestCase
from requests_test.speakers import speakers


class SpeakersTest(TestCase):
    def test_ukrainians(self):
        data = speakers("uk")
        self.assertEqual(data, {'uk': 42836922})

    def test_not_existing_language(self):
        data = speakers("mmm")
        self.assertEqual(data, {"mmm": 0})

    def test_not_string(self):
        self.assertRaises(ValueError, speakers, 1)
