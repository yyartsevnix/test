import logging
import re, pdb


class LogError(Exception):
    pass


class LogCriticalError(LogError):
    def __init__(self, line_number):
        self.strerror = f"Critical error found in line {line_number}"


ylogger = logging.getLogger(__name__)
ylogger.setLevel(logging.INFO)
FORMATTER = logging.Formatter("%(asctime)s: %(name)-20s %(levelname)-8s log string:%(message)s")

CONSOLE_HANDLER = logging.StreamHandler()
CONSOLE_HANDLER.setLevel(logging.CRITICAL)
CONSOLE_HANDLER.setFormatter(FORMATTER)

FILE_HANDLER = logging.FileHandler("found_errors.log")
FILE_HANDLER.setLevel(logging.ERROR)
FILE_HANDLER.setFormatter(FORMATTER)

ylogger.addHandler(FILE_HANDLER)
ylogger.addHandler(CONSOLE_HANDLER)


def get_critical_err(filename: str):
    try:
        with open(filename) as in_log:
            log_lines = in_log.readlines()
    except FileNotFoundError:
        return "No logfile found"
    ylogger.info("Start parsing")
    errors_num = 0
    lines = 0
    for line in log_lines:
        lines += 1
        error_match = re.search(r"\serror[\s:]", line, re.IGNORECASE)
        # pdb.set_trace()

        if error_match:
            errors_num += 1
            if re.search("critical error", line, re.IGNORECASE):
                try:
                    raise LogCriticalError(lines)
                except LogCriticalError as error:
                    ylogger.critical(error.strerror)
                    ylogger.critical(line)
            else:
                ylogger.error(line)
    try:
        out = lines / errors_num
        print("Lines per error line: ", end="")
        return out
    except ZeroDivisionError:
        return "No errors found"


if __name__ == "__main__":
    print(get_critical_err("yupdate.log"))
