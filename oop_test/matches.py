class MatchResults:

    @staticmethod
    def __check_value(value):   # maybe it's redundant
        try:
            value = int(value)
        except ValueError:
            raise ValueError("Parameters must be ints")
        if value < 0:
            raise ValueError("Parameters must be not-negative")
        return value

    def __init__(self, *, victories=0, standoffs=0,
                 fails=0, goals=0, missed=0):
        self.victories = self.__check_value(victories)
        self.standoffs = self.__check_value(standoffs)
        self.fails = self.__check_value(fails)
        self.goals = self.__check_value(goals)
        self.missed = self.__check_value(missed)

    def update(self, goals: int, missed: int):
        goals = self.__check_value(goals)
        missed = self.__check_value(missed)
        self.goals += goals
        self.missed += missed
        if goals > missed:
            self.victories += 1
        elif goals == missed:
            self.standoffs += 1
        else:
            self.fails += 1

    @property
    def points(self):
        return self.victories * 3 + self.standoffs

    @property
    def goals_score(self):
        return self.goals - self.missed

    def __str__(self):
        return f"""Statistics: 
          victories:\t\t{self.victories}
          standoffs:\t\t{self.standoffs}
          fails:\t\t{self.fails}
          goals:\t\t{self.goals}
          missed:\t\t{self.missed}
          points:\t\t{self.points}
          goals score:\t\t{self.goals_score}
        """


class MatchStatistics(MatchResults):
    @property
    def games_played(self):
        return self.victories + self.standoffs + self.fails

    def __str__(self):
        log = super(MatchStatistics, self).__str__()
        return log + f"  games played:\t\t{self.games_played}\n"


if __name__ == "__main__":
    team_result = MatchResults()
    print(team_result)
    team_result.update(goals=3, missed=1)
    print(team_result)
    team_result.update(goals=2, missed=3)
    print(team_result)
    team_result.update(goals=1, missed=1)
    print(team_result)

    print("_" * 20)

    team_statistics = MatchStatistics()
    print(team_statistics)
    team_statistics.update(goals=4, missed=3)
    print(team_statistics)
    team_statistics.update(1, 3)
    print(team_statistics)
    team_statistics.update(missed=0, goals=0)
    print(team_statistics)
    team_statistics.update(-2, 3) # Exception must be raised
