import pytz
import datetime


class CitiesTimeDelta:
    cities_and_timezones = {i.split("/")[-1]: i for i in pytz.all_timezones  # getting cities' names from
                            if ("/" in i and i.split("/")[-1].isalpha())}  # string like Europe/Riga

    time_format = "%b %d %Y %H:%M:%S"

    def __init__(self, city1, city2):
        self.city1 = city1
        self.city2 = city2
        self.city1_timezone_name = self.cities_and_timezones.get(city1)
        self.city2_timezone_name = self.cities_and_timezones.get(city2)
        self.city1_timezone = None
        self.city2_timezone = None
        self.city1_time = None
        self.city2_time = None
        self.time_difference = None

    def calculate_time(self):
        self.city1_timezone = pytz.timezone(self.city1_timezone_name)
        self.city2_timezone = pytz.timezone(self.city2_timezone_name)
        self.city1_time = datetime.datetime.now(tz=self.city1_timezone)
        self.city2_time = datetime.datetime.now(tz=self.city2_timezone)
        self.time_difference = self.city1_time.hour - self.city2_time.hour

    @property
    def city1_strtime(self):
        return f"Time in {self.city1}: {self.city1_time}\n"

    @property
    def city2_strtime(self):
        return f"Time in {self.city2}: {self.city2_time}\n"

    @property
    def str_time_diff(self):
        return f"Time difference is {self.time_difference}\n"

    def __str__(self):
        return "".join((self.city1_strtime, self.city2_strtime, self.str_time_diff))


def cities_timedelta(city1_name, city2_name):
    out = CitiesTimeDelta(city1_name, city2_name)
    if out.city1_timezone is None:
        raise ValueError(f"Unavailable timezone: {out.city1}")
    elif out.city2_timezone is None:
        raise ValueError(f"Unavailable timezone: {out.city2}")
    else:
        out.calculate_time()
        return out


if __name__ == "__main__":
    kiev_paris_diff = cities_timedelta("Sidney", "Kiev")
    print(kiev_paris_diff)
