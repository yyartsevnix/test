import datetime


FRIDAY_WEEKDAY = 4

today = datetime.datetime.utcnow().date()
current_weekday = today.weekday()

if FRIDAY_WEEKDAY >= current_weekday:   # two cases - day before friday or after
    days_to_next_friday = datetime.timedelta(days=(FRIDAY_WEEKDAY - current_weekday))
else:
    days_to_next_friday = datetime.timedelta(days=7 - (current_weekday - FRIDAY_WEEKDAY))

time_format = "%B %d, %Y"
first_friday = today + days_to_next_friday
fridays13 = []
current_friday = first_friday
if first_friday.day == 13:
    fridays13.append(first_friday.strftime(time_format))


while len(fridays13) < 10:
    current_friday = current_friday + datetime.timedelta(days=7)
    if current_friday.day == 13:
        fridays13.append(current_friday.strftime(time_format))

print("\n".join(fridays13))
