from collections import namedtuple
from datetime import datetime


AgeAndZodiak = namedtuple("AgeAndZodiak", ["age", "zodiak"])


def age_and_astrosign(birth_date_promt):
    """Date prompt must be like 29-07-1988"""
    birthday = datetime.strptime(birth_date_promt, "%d-%m-%Y").date()
    today = datetime.now().date()
    if birthday > today:
        raise ValueError("invalid birth date")
    age = today.year - birthday.year
    if today.month < birthday.month or (today.month == birthday.month and today.day < birthday.day):
        age -= 1
    astrosigns = ("Capricorn", "Aquarius", "Pisces",
                  "Aries", "Taurus", "Gemini",
                  "Cancer", "Leo", "Virgo",
                  "Libra", "Scorpio", "Sagittarius",)
    sign = astrosigns[birthday.month-1] if birthday.day < 22 else astrosigns[birthday.month]    # rough variant
    return AgeAndZodiak(age, sign)


if __name__ == "__main__":
    print(age_and_astrosign("02-08-1994"))
