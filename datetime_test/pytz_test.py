import pytz
import datetime


tz_berlin = "Europe/Berlin"

tz_GMT5 = "Etc/GMT+5"

tz_turkey = "Turkey"

server_time = datetime.datetime.utcnow()
berlin_time = pytz.timezone(tz_berlin).fromutc(server_time)
GMT3_time = pytz.timezone(tz_GMT5).fromutc(server_time)
turkey_time = pytz.timezone(tz_turkey).fromutc(server_time)
print(server_time)
print(berlin_time)
print(GMT3_time)
print(turkey_time)
