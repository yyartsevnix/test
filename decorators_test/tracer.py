from functools import wraps
import logging

funclogger = logging.getLogger(__name__)
funclogger.setLevel(logging.INFO)
FORMATTER = logging.Formatter("%(asctime)s: %(name)-15s: %(levelname)-8s %(message)s")


def functracer(logfile: str):
    def tracer(func):
        try:
            inner_handler = logging.FileHandler(logfile)
            inner_handler.setLevel(logging.INFO)
            inner_handler.setFormatter(FORMATTER)
        except TypeError:
            print("Unable to create file")
            inner_handler = logging.StreamHandler()
            inner_handler.setLevel(logging.INFO)
            inner_handler.setFormatter(FORMATTER)

        @wraps(func)
        def inner(*args, **kwargs):
            out = func(*args, **kwargs)
            arglist = [str(arg) for arg in args]
            kwrlist = [f"{kw}={kwargs[kw]}" for kw in kwargs]
            arg_str = "with no arguments " if arglist == [] \
                else f"with arguments {', '.join(arglist)}"
            kwr_str = "and no keywords; " if kwrlist == [] \
                else f"and keywords {', '.join(arglist)};"
            funclogger.addHandler(inner_handler)
            funclogger.info(" ".join((f"Calling function {func.__name__}",
                                      arg_str,
                                      kwr_str,
                                      f"returns {out}")))
            funclogger.removeHandler(inner_handler)
            return out

        return inner

    return tracer


if __name__ == "__main__":
    logfile_name = "functrace.log"
    with open(logfile_name, "w") as log:
        pass

    @functracer(logfile_name)
    def do_nothing():
        pass


    dummy_list = []


    @functracer(dummy_list)     # Checks illegal variable for logfile
    def other_nothing():
        pass


    @functracer(logfile_name)
    def useless(obj):
        return obj


    @functracer(logfile_name)
    def listy(*args):
        return list(args)

    @functracer(logfile_name)
    def kwgetter(*args, **kwargs):
        kwargs.update({i[1]:i[0] for i in enumerate(args)})
        return kwargs


    do_nothing()
    other_nothing()
    useless(14)
    listy("never", "gonna", "give")
    kwgetter("a", "b", "c", d=4, e=5)
    print(other_nothing.__name__)
