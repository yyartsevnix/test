import functools


cyrillic = {'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd',
            'е': 'e', 'ё': 'ye', 'ж': 'zh', 'з': 'z', 'и': 'i',
            'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
            'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't',
            'у': 'u', 'ф': 'f', 'х': 'kh', 'ц': 'ts', 'ч': 'ch',
            'ш': 'sh', 'щ': 'shch', 'ъ': '', 'ы': 'y', 'ь': '\'',
            'э': 'e', 'ю': 'yu', 'я': 'ya'}

cyrillic_uppercase = {char.upper(): cyrillic[char].capitalize() for char in cyrillic}
cyrillic.update(cyrillic_uppercase)


def transliterator(func):
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        old_out = func(*args, **kwargs)
        if isinstance(old_out, str):
            return "".join([cyrillic.get(char, char) for char in old_out])
        return old_out

    return wrapped


if __name__ == "__main__":
    @transliterator
    def divine_comedy_ru():
        return "\n".join(("Земную жизнь пройдя до половины",
                          "Я очутился в сумрачном лесу",
                          "Утратив правый путь во тьме долины\n"))

    @transliterator
    def return_one():
        return 1

    @transliterator
    def divine_comedy_en():
        return "\n".join(("Midway upon the journey of our life",
                          "I found myself within a forest dark,",
                          "For the straightforward pathway had been lost\n"))

    print(divine_comedy_ru())

    print(return_one())

    print(divine_comedy_en())

    print(divine_comedy_ru.__name__)
