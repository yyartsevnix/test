import requests


def is_strlist(input_list):
    try:
        listing = tuple(input_list)
    except TypeError:
        return False
    else:
        for_check = map(lambda x: isinstance(x, str), listing)
        return all(for_check)


def speakers(*input_values):
    if is_strlist(input_values):
        languages_list = list(input_values)
    else:
        raise ValueError("Only string values are acceptable")
    custom_headers = requests.utils.default_headers()
    custom_headers["User-Agent"] = "'Python Learning Requests'"
    speakers_statistics = {}
    for language in languages_list:
        response = requests.get(f"https://restcountries.eu/rest/v1/lang/{language}",
                                headers=custom_headers)
        langs_data = response.json()
        if not isinstance(langs_data, list):
            speakers_statistics[language] = 0
        else:
            speakers_number = sum([state.get("population", 0) for state in langs_data])
            speakers_statistics[language] = speakers_number
    return speakers_statistics


if __name__ == "__main__":
    print(speakers("uk"))
    print(speakers("be", "uk"))
    print(speakers("uk", "by"))
    print(speakers("en", "uk"))
    print(speakers(12))
