import requests


def all_languages_codes():
    response = requests.get("https://restcountries.eu/rest/v1/all")
    countries_data = response.json()
    languages_all = set()
    for country in countries_data:
        languages_all.update(country.get("languages"))
    return sorted(list(languages_all))


if __name__ == "__main__":
    print(all_languages_codes())
