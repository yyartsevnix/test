import zeep


def get_change_set(num):
    if not isinstance(num, int):

        print("Converting \"num\" to int")
        try:
            old, num = num, int(num)
        except ValueError:
            raise ValueError("Unable to convert input to int")
        print(f"{old} converted to {num}")
    if num < 0:
        raise ValueError("Sample object paramether must be not-negative")
    client = zeep.Client("http://secure.smartbearsoftware.com/samples/testcomplete10/webservices/Service.asmx?WSDL")
    sample = client.service.GetSampleObject(num)
    sample["X"], sample["Y"] = sample["Y"], sample["X"]
    sample["name"] = "My Test"
    return client.service.SetSampleObject(sample)


if __name__ == "__main__":
    print(get_change_set(3.))
