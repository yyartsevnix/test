import requests


def form_and_headers(request_body, headers):
    request_headers = requests.sessions.default_headers()
    request_headers.update(headers)
    post_request = requests.post("http://httpbin.org/post",
                                 data=request_body, headers=headers)
    response_json = post_request.json()
    return response_json.get("form", {}), post_request.headers


if __name__ == "__main__":
    post_data = {"comments": "No comments", "custemail": "gg@gg.gg",
                 "custname": "Я", "custtel": "9379992", "delivery": "11:15",
                 "size": "medium", "topping": ["bacon", "cheese"]}
    user_agent_header = {"User-Agent": "Python Learning Requests"}
    response_items = form_and_headers(post_data, user_agent_header)
    for item in response_items:
        print(item, "\n")
