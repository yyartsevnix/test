import requests
import re
from urllib import parse as urlparse


def check_inner_urls_status(root_url):
    headers = requests.utils.default_headers()
    headers["User-Agent"] = 'Python Learning Requests'
    response = requests.get(root_url, headers=headers)
    resp_html = response.text
    urlblock_re = re.search(r"ENDPOINTS(.+?)DESCRIPTION", resp_html, re.DOTALL)
    urlblock = urlblock_re.group()

    rel_urls = re.findall("href=\"(.+?)\"", urlblock)
    abs_urls = [urlparse.urljoin(response.url, url) for url in rel_urls]

    subres = {}
    for url in abs_urls:
        resp = requests.head(url, allow_redirects=False, headers=headers)
        if resp.status_code != 200:
            subres[url] = resp.status_code

    return subres


if __name__ == "__main__":
    httpbin = "https://nghttp2.org/httpbin/"
    subresources = check_inner_urls_status(httpbin)
    for i in subresources.items():
        print(f"{i[0]}: {i[1]}")
