import zeep, pdb


def get_books_xml(url):
    client = zeep.Client(url)
    client.transport.session.headers.update({"User-Agent": "Python Learning Requests"})
    xmldata = client.service.GetXmlData()
    books = xmldata.xpath("//book")
    book_id = 0
    for book in books:
        book_id += 1
        title, *_= book.xpath("./title/text()")
        genre, *_ = book.xpath("./genre/text()")
        review, *_ = book.xpath("./review/text()")
        price, *_ = book.xpath("./price/text()")
        yield {"id": book_id,
               "title": title,
               "genre": genre,
               "review": review,
               "price": price}


if __name__ == "__main__":
    soap_url = "http://secure.smartbearsoftware.com/samples/testcomplete10/webservices/Service.asmx?WSDL"
    books_gen = get_books_xml(soap_url)
    # pdb.set_trace()
    for book_data in books_gen:
        for i in book_data.items():
            print(f"{i[0]}: {i[1]}")
        print("\n")
